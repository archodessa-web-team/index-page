$(function(){
    // define buttons
    var langBtn = {
        ru: $('#ru-lang-btn'),
        fr: $('#fr-lang-btn'),
        de: $('#de-lang-btn'),
        en: $('#en-lang-btn'),
        ua: $('#ua-lang-btn')
    };

    var langSwitchers = $('.header--langs a');
    var objectItems = $('.object-item');

    /**
     * Main controllers
     */
    var ctrl = {
        /**
         * module entry point
         */
        init: function(){
            //bind events
            langSwitchers.on('click', ctrl.handleClick );
            //set default
            langSwitchers.first().trigger('click');
        },

        /**
         * On every lang change
         * @param event
         */
        handleClick: function(event) {
            event.preventDefault();
            // current lang link is not clickable
            if( $(event.target).hasClass('active') ) return;

            // get lang to change from links atr  data-for-lang=""
            var lang = $(event.target).data('for-lang');

            objectItems.filter('.object-item--lang-'+lang).show().end()
                .not('.object-item--lang-'+lang).hide();

            // change lang switcher to selected lang
            langSwitchers.removeClass('active').filter('[data-for-lang="'+lang+'"]').addClass('active');

            // to show lazy loaded images
            setTimeout(function () {
                $(window).trigger("scroll")
            }, 200);
        }

    };

    //init this module
    ctrl.init();
});