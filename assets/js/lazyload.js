$(function(){
  function isRetina () {
    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
                    (min--moz-device-pixel-ratio: 1.5),\
                    (-o-min-device-pixel-ratio: 3/2),\
                    (min-resolution: 1.5dppx)";

    if (window.devicePixelRatio > 1)
      return true;

    if (window.matchMedia && window.matchMedia(mediaQuery).matches)
      return true;

    return false;
  };

  var lazy = $("img.lazy");
  if (isRetina()) {
    lazy.each(function () {
      $(this).attr("data-original", $(this).data("original2x")).data("original", $(this).data("original2x"));
    });
  }

  setTimeout(function () {
    lazy.lazyload({
      effect: "fadeIn",
      skip_invisible : true
    });
  }, 3000);
});

