<?php snippet('html_header') ?>
<body class='index'>
  <noscript>
    <style>
    .emerge {
        opacity: 1;
    }
    .header--langs, .search-form, .lazy.object-item--image {
      display: none;
    }
    </style>
  </noscript>
  <?php snippet('header') ?>

  <?php snippet('sidebar') ?>

  <?php snippet('footer') ?>

  <main>
    <?php snippet('searchform') ?>

    <div class='emerge' data-effect='relax' data-duration="6000">
      <?php $data = $page->children()->sortBy('modified', 'desc'); ?>
      <div class="object-list">
        <?php foreach($data as $article): ?>
          <?php if(!$article->image() || !$article->locales()) { continue; } ?>
          <?php
            $thumb = thumb($article->image(), array('width' => 256, 'height' => 256, 'crop' => true, 'quality' => 80));
            $thumb2x = thumb($article->image(), array('width' => 512, 'height' => 512, 'crop' => true, 'quality' => 50));
          ?>
          <?php foreach($article->locales()->toStructure() as $locale): ?>
            <?php snippet('article-block', array(
              'link' => createLangLink((string)$locale->language(), $article->link()),
              'language' => (string)$locale->language(),
              'title' => (string)$locale->title(),
              'address' => (string)$locale->address(),
              'alt' => (string)$locale->alt(),
              'image' => $thumb->url(),
              'image2x' => $thumb2x->url()
            )); ?>
          <?php endforeach ?>
        <?php endforeach ?>

        <?php foreach($page->file('objects.json')->read('json') as $article): ?>
          <?php snippet('article-block', array(
            'link' => (string)$article['url'],
            'language' => (string)$article['lang'],
            'title' => (string)$article['name'],
            'address' => (string)$article['dev'],
            'alt' => (string)$article['alt'],
            'image' => 'http://archodessa.com'.$article['img'],
            'image2x' => 'http://archodessa.com'.str_replace('.jpg', '@2x.jpg', $article['img'])
          )); ?>
        <?php endforeach ?>
      </div>
    </div>
  </main>
<?php snippet('html_footer') ?>