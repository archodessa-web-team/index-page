<?php
$ignore = array('sitemap', 'error');

// send the right header
header('Content-type: text/xml; charset="utf-8"');

// echo the doctype
echo '<?xml version="1.0" encoding="utf-8"?>';

?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php $indexPage = $pages->find('home'); ?>

   <?php snippet('xml-block', array(
      'loc' => $indexPage->url(),
      'lastmod' => $indexPage->modified('c'),
      'changefreq' => 'weekly',
      'priority' => 1
    )); ?>

    <?php foreach($indexPage->children()->sortBy('modified', 'desc') as $article): ?>
      <?php if(!$article->image() || !$article->locales()) { continue; } ?>
      <?php foreach($article->locales()->toStructure() as $locale): ?>
       <?php snippet('xml-block', array(
          'loc' => createLangLink((string)$locale->language(), $article->link()),
          'lastmod' => $article->modified('c'),
          'priority' => 0.9
        )); ?>
      <?php endforeach ?>
    <?php endforeach ?>


    <?php foreach($indexPage->file('objects.json')->read('json') as $article): ?>
       <?php snippet('xml-block', array(
          'loc' => 'http://archodessa.com/'.$article['url'],
          'priority' => 0.9
        )); ?>
    <?php endforeach ?>
</urlset>

