<?php if(!defined('KIRBY')) exit ?>

username: admin
email: seigiard@gmail.com
password: >
  $2a$10$k8DdhyIj3WmPP.UWdRWEMe7uCw8027Q0JqDCIRfxa61s2bRvJQM1W
language: ru
role: admin
history:
  - home/object2
  - home/object3
  - home/aleksandrovskiy-prospekt-5
  - home
