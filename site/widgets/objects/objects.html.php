<ul class="nav nav-list sidebar-list">
  <?php foreach($pages as $subpage): ?>
  <li>
      <a class="draggable<?php e($subpage->isInvisible(), ' invisible'); ?>" data-helper="<?php __($subpage->title(), 'attr') ?>" data-text="<?php __($subpage->dragText()) ?>" href="<?php __($subpage->url('edit')) ?>">
        <?php echo $subpage->icon() ?><span><?php __($subpage->title()) ?><?php foreach($subpage->locales()->toStructure() as $locale): ?>, <span  class="language-marker"><?php echo (string)$locale->language(); ?></span><?php endforeach ?>
        </span>
        <small class="marginalia shiv shiv-left shiv-white"><?php __($subpage->modified('d/m/Y H:i')) ?></small>
      </a>
      <a class="option" data-context="<?php __($subpage->url('context')) ?>" href="#options"><?php i('ellipsis-h') ?></a>
    </li>
  <?php endforeach ?>
</ul>