<?php

$page    = panel()->site()->page('home');
$options = array();


// if($page->canHaveSubpages()) {
//   $options[] = array(
//     'text' => 'Сортировать', //l('dashboard.index.pages.edit'),
//     'icon' => 'list',
//     'link' => $page->url('subpages')
//   );
// }

if($addbutton = $page->addButton()) {
  $options[] = array(
    'text'  => l('dashboard.index.pages.add'),
    'icon'  => 'plus-circle',
    'link'  => $addbutton->url(),
    'modal' => $addbutton->modal(),
    'key'   => '+',
  );
}

return array(
  'title' => array(
    'text'       => 'Объекты', // l('dashboard.index.pages.title'),
    'link'       => $page->url('edit'),
    'compressed' => true
  ),
  'options' => $options,
  'html'  => function() use($page) {
    return tpl::load(__DIR__ . DS . 'objects.html.php', array(
      'pages' => $page->children()->sortBy('modified', 'desc')->paginated('sidebar')
    ));
  }
);