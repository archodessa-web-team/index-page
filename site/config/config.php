<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'put your license key here');

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/
c::set('cache',true);
c::set('cache.ignore', array('sitemap_xml'));

c::set('panel.language', 'ru');

c::set('panel.stylesheet', 'assets/css/panel.css');

c::set('panel.widgets', array(
  'objects'    => true,
  'pages'    => false,
  'site'     => false,
  'account'  => false,
  'history'  => false
));

Dir::$defaults['permissions'] = 0775;

function setPermissionsForPage ($page) {
    @chmod($page->root(), 0775);
    foreach ($page->files() as $key => $file) {
      @chmod($file->root(), 0777);
    }
}

function generateSitemapXml() {
    $kirby = new Kirby();
    $sitemapPath = kirby()->roots()->index().'/../sitemap.xml';
    file_put_contents($sitemapPath, $kirby->render( page('sitemap') ));
    @chmod($sitemapPath, 0777);
}

kirby()->hook('panel.page.create', function($page) {
    setPermissionsForPage($page);
    generateSitemapXml();
});

kirby()->hook('panel.page.update', function($page) {
    setPermissionsForPage($page);
    generateSitemapXml();
});