<?php if(!defined('KIRBY')) exit ?>

title: Объект
pages: false
icon: home
preview: parent
options:
  status: false
  template: false
  url: false
files:
    type:
        - image
    max: 1
fields:
  title:
    label: Название объекта
    type:  text
    required: true
  link:
    label: Ссылка на статью
    type:  url
    required: true
  image:
    label: Изображение
    type:  selector
    mode:  single
    types:
        - image
    autoselect: first
    required: true
    help: Обрежем до 256×256 и 512×512 пикселей
  locales:
    label: Доступные переводы
    type: structure
    entry: >
      <span class="language-marker" style="margin-right:2em">{{language}}</span> {{title}}, <em>{{address}}</em>
    modalsize: large
    fields:
      language:
        label: Язык
        type: select
        required: true
        options:
          ru: Русский
          ua: Украинский
          en: Английский
          de: Немецкий
          fr: Французский
      title:
        label: Заголовок
        type: text
        required: true
      address:
        label: Адрес
        type: text
        required: true
      alt:
        label: Текст к изображению
        type: text
        required: true