<?php if(!defined('KIRBY')) exit ?>

title: Simple Page
pages: true
files: true
fields:
  title:
    label: Title
    type:  text
  menu:
    label: Menu Title
    type:  text
  text:
    label: Text
    type:  textarea