<?php if(!defined('KIRBY')) exit ?>

title: Main Page
pages:
  template: object
options:
  preview: true
  status: false
  template: false
  url: true
  delete: false
files: true
fields:
  title:
    label: Название раздела
    type:  text
    required: true