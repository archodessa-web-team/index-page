<header class="header">
  <h1 class="header--title">
    <a href='/' id='logo' title='Архитектура Одессы' class='emerge header--logo' data-effect='zoom' data-duration='3000'>
      <img alt='Архитектура Одессы' width='128' height='76'
        src='<?php echo url("/assets/images/retinapps@2x.png"); ?>'
        title='Архитектура Одессы'/>
      Архитектура <strong>Одессы</strong>
    </a>
  </h1>

  <nav id="langs-switcher" class="emerge header--langs" data-await="logo" data-hold="1500" data-effect='relax' data-scale=".5" data-origin="bottom" data-duration="1000">
    <a class="lang-btn" href="" data-for-lang="ru">Рус</a>
    <a class="lang-btn" href="" data-for-lang="ua">Укр</a>
    <a class="lang-btn" href="" data-for-lang="en">Eng</a>
    <a class="lang-btn" href="" data-for-lang="fr">Fra</a>
    <a class="lang-btn" href="" data-for-lang="de">De</a>
  </nav>
</header>

