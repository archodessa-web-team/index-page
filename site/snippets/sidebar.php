<aside class="sidebar">
    <ul class='aside-list emerge' id='menu' data-await="langs-switcher" data-hold="300" data-effect='slide' data-down="2em" data-origin="bottom" data-duration="1000">
        <li><a title="О проекте «Архитектура Одессы»" href="all/project/">О проекте</a></li>
        <li><a title="Помочь проекту «Архитектура Одессы»" href="all/patrons/">Помочь</a></li>
        <li><a title="Партнёры проекта «Архитектура Одессы»" href="all/partners/">Партнёры</a></li>
        <li><a title="Отзывы о проекте «Архитектура Одессы»" href="all/feedbacks/">Отзывы</a></li>
        <li>&nbsp;<!-- --></li>
        <li><a title="Экскурсии по архитектуре Одессы" href="http://tours.archodessa.com">Экскурсии</a></li>
        <li><a title="Словарь архитектурно-реставрационных терминов Одессы" href="http://terms.archodessa.com">Термины</a></li>
        <li>&nbsp;<!-- --></li>
        <li><i><a href="all/donate/">Меценаты</a></i></li>
    </ul>
</aside>

