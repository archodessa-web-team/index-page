<!--[if lt IE 7 ]> <html class="ie6 lt-ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9 lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en">
<!-- <![endif]-->
  <head>
    <meta charset="utf-8" />
    <base href="<?php echo $site->url(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="robots" content="index, follow"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="<?php echo $site->description() ?>"/>

    <meta http-equiv='x-dns-prefetch-control' content='on'>
    <link rel='dns-prefetch' href='https://mc.yandex.ru/'>
    <link rel='dns-prefetch' href='//www.google-analytics.com'>

    <title><?php echo $site->title(); ?></title>
    <?php snippet('_sys-metatags') ?>

    <!-- CSS internal -->
    <?php echo css('assets/css/all.css'); ?>
    <?php echo css('assets/css/fonts.css'); ?>

    <!-- JS -->

    <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter24644945 = new Ya.Metrika({ id:24644945, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div></div></noscript> <!-- /Yandex.Metrika counter -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-49731826-1', 'auto');
      ga('send'

        , 'pageview');
    </script>

    <script>
      /* Google Analytics */
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-34184058-1']);
      _gaq.push(['_trackPageview']);

      (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
      })();
      /* Google Analytics */
    </script>
  </head>