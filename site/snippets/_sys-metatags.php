<?php
    /*
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo url('/assets/images/favicons/apple-touch-icon-57x57.png'); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo url('/assets/images/favicons/apple-touch-icon-60x60.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo url('/assets/images/favicons/apple-touch-icon-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo url('/assets/images/favicons/apple-touch-icon-76x76.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo url('/assets/images/favicons/apple-touch-icon-114x114.png'); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo url('/assets/images/favicons/apple-touch-icon-120x120.png'); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo url('/assets/images/favicons/apple-touch-icon-144x144.png'); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo url('/assets/images/favicons/apple-touch-icon-152x152.png'); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo url('/assets/images/favicons/apple-touch-icon-180x180.png'); ?>">
<link rel="icon" type="image/png" href="<?php echo url('/assets/images/favicons/favicon-32x32.png'); ?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo url('/assets/images/favicons/favicon-194x194.png'); ?>" sizes="194x194">
<link rel="icon" type="image/png" href="<?php echo url('/assets/images/favicons/favicon-96x96.png'); ?>" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo url('/assets/images/favicons/android-chrome-192x192.png'); ?>" sizes="192x192">
<link rel="icon" type="image/png" href="<?php echo url('/assets/images/favicons/favicon-16x16.png'); ?>" sizes="16x16">
<link rel="manifest" href="<?php echo url('/assets/images/favicons/manifest.json'); ?>">
<link rel="mask-icon" href="<?php echo url('/assets/images/favicons/safari-pinned-tab.svg'); ?>" color="#5bbad5">
<link rel="shortcut icon" href="<?php echo url('/assets/images/favicons/favicon.ico'); ?>">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-TileImage" content="<?php echo url('/assets/images/favicons/mstile-144x144.png'); ?>">
<meta name="msapplication-config" content="<?php echo url('/assets/images/favicons/browserconfig.xml'); ?>">
<meta name="theme-color" content="#ffffff">
*/ ?>
<meta name="robots" content="index, follow" />

<link rel="index" id="link-index" href="http://archodessa.com/"/>
<meta property="og:title" content="Архитектура Одессы. Два века каменной летописи"/>
<meta property="og:site_name" content="<?php echo html($site->title()) ?>"/>
<meta property="og:type" content="website" />
<meta property="og:description" content="<?php echo html($site->description()) ?>"/>
<meta name="description" content="<?php echo html($site->description()) ?>" />
<meta itemprop="description" content="<?php echo html($site->description()) ?>" />

<meta content="/images/Icon-256.png" property="og:image"/>
<meta property="og:image" content="<?php echo url('assets/images/logo.png') ?>"/>
<meta itemprop="image" content="<?php echo url('assets/images/logo.png') ?>"/>
<link rel="image_src" href="<?php echo url('assets/images/logo.png') ?>" />