<?php
function getMenuTitle($page){
    $title = $page->content()->has('menu') && trim($page->menu()) != "" ? $page->menu() : $page->title();
    return $title->html();
}
?>

<nav class="nav" role="navigation">
  <?php foreach($pages->visible() AS $p): ?>
  <a class="nav__link <?php echo ($p->isOpen()) ? 'active-trail' : '' ?>" href="<?php echo $p->url() ?>"><?php echo getMenuTitle($p); ?></a>
  <?php endforeach ?>

  <?php if($user = $site->user()): ?>
    <a class="nav__link nav__link--logout" href="<?php echo url('logout') ?>">Logout</a>
  <?php endif ?>
</nav>