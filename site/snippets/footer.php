<footer class="footer">
    <div class="emerge footer--social" id='social' data-await="menu" data-hold="800" data-effect='slide' data-up="2em">
        <div class="likely likely-small">
            <div class="facebook">Поделиться</div>
            <div class="twitter">Твитнуть</div>
        </div>
    </div>

    <ul class='aside-list emerge' id='creators2' data-await="social" data-hold="300" data-effect='slide' data-up="2em" data-origin="bottom" data-duration="1000">
        <li><i>Создатели:</i></li>
        <li><a href='authors/levitsky/index.html' title='Арт-директор, худрук, фотограф и колорист'>Александр
                Левицкий</a></li>
        <li><a href='authors/shamatazhi/index.html' title='Фотограф и составитель'>Дмитрий Шаматажи</a></li>
        <li>&nbsp;<!-- --></li>
        <li>&copy; <span style="color:rgb(51,51,51)">1794&mdash;2016</span></li>
    </ul>
</footer>