<article class="object-list--item object-item object-item--lang-<?php echo $language; ?>">
  <a class="object-item--link" href='<?php echo $link; ?>'>
      <img
        src='<?php echo url("/assets/images/space.png"); ?>'
        data-original='<?php echo $image; ?>'
        data-original2x='<?php echo $image2x; ?>'
        alt='<?php echo $alt; ?>'
        class='lazy object-item--image' width='256' height='256'
      />
      <noscript>
        <img
          src='<?php echo $image; ?>'
          alt='<?php echo $alt; ?>'
          class='object-item--image' width="256" height="256"
        />
      </noscript>
      <h3 class="object-item--title"><?php echo $title; ?></h3>
      <p class="object-item--address"><?php echo $address; ?></p>
  </a>
</article>