<?php

function createLangLink ($language, $link)
{
    if($language == 'ru'){
        return $link;
    }

    if(substr($link, -1) === '/') {
        return substr($link, 0, -1).'-'.$language.'/';
    }

    return $language.'-'.$link;
}
